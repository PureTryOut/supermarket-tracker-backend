#!/bin/sh

set -e


if [ ! -n "$SKIP_SETUP" ] || [ "$SKIP_SETUP" = "false" ]; then
    python3 manage.py migrate
    python3 manage.py collectstatic --no-input
fi

exec "$@"