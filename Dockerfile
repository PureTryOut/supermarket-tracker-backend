FROM docker.io/python:3.12-alpine AS builder

ARG ENVIRONMENT

# Determine if dev dependencies need to be installed
ENV ENVIRONMENT=${ENVIRONMENT}
# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE 1
# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED 1
# Lock Poetry version to make the dependency installation reproducible
ENV POETRY_VERSION=1.5.0

RUN apk add --no-cache \
      build-base \
      git \
      libffi-dev \
      postgresql16-dev
RUN pip install "poetry==$POETRY_VERSION"

WORKDIR /src
COPY poetry.lock pyproject.toml README.md ./

# Project initialization
RUN poetry config virtualenvs.in-project true && \
    poetry install $(test "$ENVIRONMENT" == production && echo "--only=main") --no-interaction --no-ansi --no-root && \
    poetry run pip install uvicorn[standard]

FROM docker.io/python:3.12-alpine
WORKDIR /app

# Copy the virtual environment from the builder
COPY --from=builder /src/.venv /venv

RUN addgroup -g 1000 -S app && \
  adduser -s /bin/sh -D -H -S -G app -u 1000 app
RUN mkdir /static /app-media /var/log/supermarket_tracker && \
  chown app:app /static && \
  chown app:app /app-media && \
  chown app:app /var/log/supermarket_tracker

# Install dependencies for the deps in the virtual environment
# postgresql-bdr-client is installed to make sure `psql` is available in the container
# That's useful to be able to run `./manage.py dbshell` in the container
RUN apk add --no-cache \
  libpq \
  freetype \
  libffi \
  cairo \
  gettext \
  postgresql-bdr-client

# Copy the actual application source code
COPY . /app

# Activate the virtual environment
ENV PATH="/venv/bin:${PATH}"
ENV VIRTUAL_ENV="/venv"

USER app

# Set application variables
ENV DJANGO_SETTINGS_MODULE=supermarket_tracker_backend.settings.dev
ENV DB_NAME="db.sqlite3"
ENV SKIP_SETUP=false
ENV STATIC_ROOT="/static"
ENV MEDIA_ROOT="/files"
ENV FILE_STORAGE_LOCATION="/files"

# Expose volumes that can be mounted
VOLUME /static
VOLUME /files
VOLUME /var/log/supermarket_tracker

EXPOSE 8000

CMD python3 -m uvicorn \
    --host 0.0.0.0 \
    --port 8000 \
    --forwarded-allow-ips "*" \
    --log-level 'warning' \
    supermarket_tracker_backend.asgi:application
ENTRYPOINT ["/app/entrypoint.sh"]
