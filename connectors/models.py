from dataclasses import dataclass
from enum import Enum
from typing import Optional


class Stores(Enum):
    ALBERT_HEIJN: str = "ALBERT_HEIJN"
    JUMBO: str = "JUMBO"
    COOP: str = "COOP"
    ALDI: str = "ALDI"


class CategoryOptions(Enum):
    FRUIT_AND_VEGETABLES: int = "FRUIT_AND_VEGETABLES"
    BREAD_AND_PASTRIES: int = "BREAD_AND_PASTRIES"
    BEVERAGES: int = "BEVERAGES"
    REFRIGERATED_GOOD: int = "REFRIGERATED_GOOD"
    FROZEN: int = "FROZEN"
    STAPLE_FOOD: int = "STAPLE_FOOD"
    SWEET_AND_SALTY: int = "SWEET_AND_SALTY"
    CARE: int = "CARE"
    HOUSEHOLD_AND_DOMESTIC_ANIMALS: int = "HOUSEHOLD_AND_DOMESTIC_ANIMALS"
    UNKNOWN: int = "UNKNOWN"


@dataclass
class NormalizedCategory:
    id: str
    category: CategoryOptions
    shop_id: Optional[str] = None


@dataclass
class NormalizedProduct:
    store: Stores
    id: str
    name: str
    price: int
    unit: str
    """
    Since parsing manually inserted unit strings into machine-comparable ones
    is very hard and probably goes wrong often, it's good to keep a reference
    to what they store in the shop
    """
    shop_unit_str: str
    quantity: float
    category: NormalizedCategory
    """
    Should be a full, absolute URL
    """
    image_url: Optional[str] = None
    ean_code: Optional[str] = None
