from ..stores.ah import split_unit_and_quantity


def test_split_unit_and_quantity():
    splitted_value = split_unit_and_quantity("ca 3 l")
    assert splitted_value["quantity"] == 3
    assert splitted_value["unit"] == "l"

    splitted_value = split_unit_and_quantity("3x25 g")
    assert splitted_value["quantity"] == 3 * 25
    assert splitted_value["unit"] == "g"

    splitted_value = split_unit_and_quantity("3x25g")
    assert splitted_value["quantity"] == 3 * 25
    assert splitted_value["unit"] == "g"

    splitted_value = split_unit_and_quantity("24x 0.25 l")
    assert splitted_value["quantity"] == 24 * 0.25
    assert splitted_value["unit"] == "l"

    splitted_value = split_unit_and_quantity("4x 0.25 l")
    assert splitted_value["quantity"] == 4 * 0.25
    assert splitted_value["unit"] == "l"

    splitted_value = split_unit_and_quantity("3x 100g")
    assert splitted_value["quantity"] == 3 * 100
    assert splitted_value["unit"] == "g"

    splitted_value = split_unit_and_quantity("3x ca 150g")
    assert splitted_value["quantity"] == 3 * 150
    assert splitted_value["unit"] == "g"

    splitted_value = split_unit_and_quantity("130")
    assert splitted_value["quantity"] == 130
    assert splitted_value["unit"] == "piece"

    splitted_value = split_unit_and_quantity("100gr")
    assert splitted_value["quantity"] == 100
    assert splitted_value["unit"] == "gr"

    splitted_value = split_unit_and_quantity("0.75l")
    assert splitted_value["quantity"] == 0.75
    assert splitted_value["unit"] == "l"

    splitted_value = split_unit_and_quantity("tros")
    assert splitted_value["quantity"] == 1
    assert splitted_value["unit"] == "tros"

    splitted_value = split_unit_and_quantity("doos")
    assert splitted_value["quantity"] == 1
    assert splitted_value["unit"] == "doos"

    splitted_value = split_unit_and_quantity("2-3 personen")
    assert splitted_value["quantity"] == 1
    assert splitted_value["unit"] == "piece"

    splitted_value = split_unit_and_quantity("los per kilo")
    assert splitted_value["quantity"] == 1
    assert splitted_value["unit"] == "kilo"

    splitted_value = split_unit_and_quantity("per 500 gram")
    assert splitted_value["quantity"] == 500
    assert splitted_value["unit"] == "gram"

    splitted_value = split_unit_and_quantity("Zak 500 g.")
    assert splitted_value["quantity"] == 500
    assert splitted_value["unit"] == "g."

    splitted_value = split_unit_and_quantity("6 x 0.15")
    assert splitted_value["quantity"] == 0.9
    assert splitted_value["unit"] == "kilo"

    splitted_value = split_unit_and_quantity("6 x 0.15", unit_price_description="liter")
    assert splitted_value["quantity"] == 0.9
    assert splitted_value["unit"] == "liter"

    splitted_value = split_unit_and_quantity("5 x 70gr")
    assert splitted_value["quantity"] == 350
    assert splitted_value["unit"] == "gr"

    splitted_value = split_unit_and_quantity("2 * 400 g")
    assert splitted_value["quantity"] == 800
    assert splitted_value["unit"] == "g"

    splitted_value = split_unit_and_quantity("los per 500 g")
    assert splitted_value["quantity"] == 500
    assert splitted_value["unit"] == "g"

    splitted_value = split_unit_and_quantity("1 m x 6 cm")
    assert splitted_value["quantity"] == 1
    assert splitted_value["unit"] == "m"

    splitted_value = split_unit_and_quantity("per pakket")
    assert splitted_value["quantity"] == 1
    assert splitted_value["unit"] == "pakket"

    splitted_value = split_unit_and_quantity("6-pack")
    assert splitted_value["quantity"] == 6
    assert splitted_value["unit"] == "piece"

    splitted_value = split_unit_and_quantity("3 wasbeurten")
    assert splitted_value["quantity"] == 1
    assert splitted_value["unit"] == "piece"

    splitted_value = split_unit_and_quantity("pet pakket")
    assert splitted_value["quantity"] == 6
    assert splitted_value["unit"] == "piece"

    splitted_value = split_unit_and_quantity("100ca. 80g g")
    assert splitted_value["quantity"] == 100 * 80
    assert splitted_value["unit"] == "g"

    splitted_value = split_unit_and_quantity("9 Pyjama pants")
    assert splitted_value["quantity"] == 9
    assert splitted_value["unit"] == "piece"

    splitted_value = split_unit_and_quantity("1 200 g")
    assert splitted_value["quantity"] == 1200
    assert splitted_value["unit"] == "g"

    splitted_value = split_unit_and_quantity("6 x 200g g")
    assert splitted_value["quantity"] == 6 * 200
    assert splitted_value["unit"] == "g"

    splitted_value = split_unit_and_quantity("150g g")
    assert splitted_value["quantity"] == 150
    assert splitted_value["unit"] == "g"

    splitted_value = split_unit_and_quantity("L l")
    assert splitted_value["quantity"] == 1
    assert splitted_value["unit"] == "l"

    splitted_value = split_unit_and_quantity("ca. 390 g")
    assert splitted_value["quantity"] == 390
    assert splitted_value["unit"] == "g"

    splitted_value = split_unit_and_quantity("500-g-Per zak")
    assert splitted_value["quantity"] == 500.0
    assert splitted_value["unit"] == "g"

    splitted_value = split_unit_and_quantity("g-500 g")
    assert splitted_value["quantity"] == 500.0
    assert splitted_value["unit"] == "g"

    splitted_value = split_unit_and_quantity("350-360 g.")
    assert splitted_value["quantity"] == 350
    assert splitted_value["unit"] == "g."

    splitted_value = split_unit_and_quantity("3 x 35-50 g.")
    assert splitted_value["quantity"] == 3 * 35
    assert splitted_value["unit"] == "g."

    splitted_value = split_unit_and_quantity("2-10-g")
    assert splitted_value["quantity"] == 2
    assert splitted_value["unit"] == "g"

    # Baby nappies from the ALDI, for some reason including target group (babies between 5 and 9 kg)
    splitted_value = split_unit_and_quantity("5 - 9 kg, 46 stuks")
    assert splitted_value["quantity"] == 46
    assert splitted_value["unit"] == "stuks"

    splitted_value = split_unit_and_quantity("6- l -Per zak")
    assert splitted_value["quantity"] == 6
    assert splitted_value["unit"] == "l"

    splitted_value = split_unit_and_quantity("12x 85g.")
    assert splitted_value["quantity"] == 12 * 85
    assert splitted_value["unit"] == "g."

    splitted_value = split_unit_and_quantity("2x350-g-Per pak")
    assert splitted_value["quantity"] == 2 * 350
    assert splitted_value["unit"] == "g"

    splitted_value = split_unit_and_quantity("6x0.33 l.")
    assert splitted_value["quantity"] == 6 * 0.33
    assert splitted_value["unit"] == "l."

    splitted_value = split_unit_and_quantity("ca. 250-g")
    assert splitted_value["quantity"] == 250
    assert splitted_value["unit"] == "g"

    splitted_value = split_unit_and_quantity("360/400-g")
    assert splitted_value["quantity"] == 360
    assert splitted_value["unit"] == "-g"

    splitted_value = split_unit_and_quantity("kg-5.90")
    assert splitted_value["quantity"] == 5.9
    assert splitted_value["unit"] == "kg"
