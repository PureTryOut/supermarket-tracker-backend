import re
from decimal import Decimal, InvalidOperation
from typing import Any

GLOBAL_UNITS = {
    "piece": {"unit": "piece", "factor": 1},
    "g": {"unit": "g", "factor": 1},
    "gr": {"unit": "g", "factor": 1},
    "gram": {"unit": "g", "factor": 1},
    "kg": {"unit": "g", "factor": 1000},
    "kilo": {"unit": "g", "factor": 1000},
    "kilogram": {"unit": "g", "factor": 1000},
    "ml": {"unit": "ml", "factor": 1},
    "cl": {"unit": "ml", "factor": 10},
    "dl": {"unit": "ml", "factor": 100},
    "l": {"unit": "ml", "factor": 1000},
    "liter": {"unit": "ml", "factor": 1000},
    "mm": {"unit": "cm", "factor": 1},
    "cm": {"unit": "mm", "factor": 10},
    "m": {"unit": "mm", "factor": 100},
    "mtr": {"unit": "mm", "factor": 100},
    "meter": {"unit": "mm", "factor": 100},
}


def __convert_unit(item, units) -> dict[str, Any]:
    if isinstance(item["quantity"], str):
        item["quantity"] = float(item["quantity"].replace(",", "."))

    unit = item["unit"]
    if isinstance(unit, str):
        unit = unit.lower()

    converted_unit = GLOBAL_UNITS[unit] if unit in GLOBAL_UNITS else units[unit]

    item["quantity"] = converted_unit["factor"] * item["quantity"]
    item["unit"] = converted_unit["unit"]
    return item


def __has_numbers(input) -> bool:
    return any(char.isdigit() for char in input)


def __has_letters(input) -> bool:
    return any(char.isalpha() for char in input)


def __split_attached_values(string: str) -> tuple[str, str]:
    # We can't easily use regex as we need to find both floats and whole numbers
    tail = ""
    for i in range(0, len(string)):
        character = string[len(string) - i - 1]
        if character.isdigit():
            # Found first digit from the end
            tail = string[: (len(string) - i)]
            break

    head = string[len(tail) :]
    return tail, head


def split_unit_and_quantity(  # pylint: disable=too-many-branches,too-many-statements
    unit_string: str,
    unit_price_description: str | None = None,
) -> dict[str, float | int | str]:
    # Dutch store unit and quantity indication is a mess.
    # It seems to be manually entered rather than using for example enums for the unit.
    # Because of that you get fun things like "350g" but also "350 g" or even " 350 g"
    # This function tries to parse it all as good as possible and give us something reasonable to work with
    # It is ugly though and definitely could use improving. Once unit tests for this function are made it should
    # be rewritten

    quantity = None
    unit = None

    if "," in unit_string:
        unit_string = unit_string.split(",")[1]

    splitted_unit_string: list = unit_string.split(" ")

    if "ca" in splitted_unit_string[0].lower():
        if splitted_unit_string[0][3:].lower() == "ca.":
            # "100ca. 80g g"
            # TODO: absolutely terrible parsing for a terrible string, needs improving
            quantity = Decimal(splitted_unit_string[0][:-3]) * Decimal(splitted_unit_string[1][:-1])
            unit = splitted_unit_string[2]
        else:
            test = unit_string.split(" ")
            if not __has_numbers(test):
                # "ca. 250-g"
                quantity, unit = test[1].split("-")
            else:
                # "ca 3 l"
                # "ca. 390 g"
                quantity = Decimal(splitted_unit_string[1])
                unit = splitted_unit_string[2]

    if "x" in splitted_unit_string[0] and len(splitted_unit_string[0]) > 2:
        multiplication = splitted_unit_string[0].split("x")

        if len(splitted_unit_string) == 1:
            # "3x25g"
            split = splitted_unit_string[0].split("x")
            splitted_value = __split_attached_values(split[1])
            multiplication[0] = split[0]
            multiplication[1] = splitted_value[0]
            unit = splitted_value[1]

        if len(splitted_unit_string) == 2:
            # "3x25 g"

            numbers = splitted_unit_string[0].split("x")
            if len(numbers[1]) == 0:
                # "12x 85g."
                test = __split_attached_values(splitted_unit_string[1])

                multiplication = [int(numbers[0]), int(test[0])]
                splitted_unit_string[1] = test[1]
                unit = splitted_unit_string[1]
            else:
                # "6x0.33 l."
                if __has_letters(numbers[1]):
                    # "2x350-g-Per pak"
                    splitted_unit_and_quantity = numbers[1].split("-")
                    multiplication[0] = numbers[0]
                    multiplication[1] = int(splitted_unit_and_quantity[0])
                    unit = splitted_unit_and_quantity[1]
                else:
                    multiplication = [float(unit) for unit in numbers]
                    unit = splitted_unit_string[1]

        if len(splitted_unit_string) == 3:
            # "24x 0.25 l"
            multiplication[0] = splitted_unit_string[0].removesuffix("x")
            multiplication[1] = splitted_unit_string[1]
            splitted_value = (
                splitted_unit_string[1],
                splitted_unit_string[2],
            )

        quantity = float(multiplication[0]) * float(multiplication[1])

    elif "x" in splitted_unit_string[0] and len(splitted_unit_string[0]) == 2:
        # "4x 0.25 l"

        multiplication = splitted_unit_string[0][:1]
        try:
            quantity = Decimal(multiplication) * Decimal(splitted_unit_string[1])
        except InvalidOperation:
            # "3x 100g"
            unit_to_split = splitted_unit_string[1]
            if len(splitted_unit_string) == 3:
                # "3x ca 150g"
                unit_to_split = splitted_unit_string[2]

            splitted_value = __split_attached_values(unit_to_split)
            quantity = Decimal(multiplication) * Decimal(splitted_value[0])
            unit = splitted_value[1]

    if len(splitted_unit_string) == 1 and unit is None:
        if splitted_unit_string[0].isdigit():
            # "130"
            quantity = Decimal(splitted_unit_string[0])
            unit = "piece"
        elif "-" in splitted_unit_string[0] and len(splitted_unit_string[0].split("-")) > 2:
            # "2-10-g"
            new_splitted_unit_string = splitted_unit_string[0].split("-")
            quantity = int(new_splitted_unit_string[0])
            unit = "g"
        elif "/" in splitted_unit_string[0]:
            # "360/400-g"
            quantity, to_parse_unit = splitted_unit_string[0].split("/")

            unit = __split_attached_values(to_parse_unit)[1]
        else:
            splitted_value = __split_attached_values(splitted_unit_string[0])
            try:
                # "100gr"
                # "0.75l"
                quantity = Decimal(splitted_value[0])
                unit = splitted_value[1]
            except InvalidOperation:
                pass

            if quantity is None:
                # "tros"/"doos"
                quantity = 1
                unit = splitted_unit_string[0].lower()
    elif len(splitted_unit_string) == 2 and unit is None:
        unit = splitted_unit_string[1]

        if splitted_unit_string[0].lower() != "per":
            quantity = splitted_unit_string[0]
        if splitted_unit_string[1].lower() == "personen":
            # "2-3 personen"
            quantity = 1
            unit = "piece"
        elif "-" in splitted_unit_string[0]:
            if splitted_unit_string[0][0].isalpha():
                # "g-500 g"

                # First find the location of the first number
                index_first_number: int
                for i, character in enumerate(splitted_unit_string[0]):
                    if character.isdigit():
                        index_first_number = i
                        break

                quantity = float(splitted_unit_string[0][index_first_number:])
            else:
                new_splitted_unit_string = splitted_unit_string[0].split("-")
                if len(new_splitted_unit_string) == 2:
                    # "350-360 g."
                    quantity = float(new_splitted_unit_string[0])
                    unit = splitted_unit_string[1]
                if len(new_splitted_unit_string) == 3:
                    # "500-g-Per zak"
                    new_unit_string = new_splitted_unit_string[0] + " " + new_splitted_unit_string[1]
                    new_unit_and_quantity = split_unit_and_quantity(new_unit_string)
                    quantity = float(new_unit_and_quantity["quantity"])
                    unit = new_unit_and_quantity["unit"]

    elif len(splitted_unit_string) == 3 and unit is None:
        if (
            not __has_letters(splitted_unit_string[0])
            and not __has_numbers(splitted_unit_string[1])
            and not __has_numbers(splitted_unit_string[2])
        ):
            # "9 Pyjama pants"
            quantity = float(splitted_unit_string[0])
            unit = "piece"

        if (
            not __has_letters(splitted_unit_string[0])
            and __has_numbers(splitted_unit_string[0])
            and __has_numbers(splitted_unit_string[1])
        ):
            # "1 200 g"
            quantity = Decimal(splitted_unit_string[0] + splitted_unit_string[1])
            unit = splitted_unit_string[2]

        if splitted_unit_string[0].lower() == "los":
            # "los per kilo"
            quantity = 1
            unit = splitted_unit_string[2]

        if not __has_numbers(splitted_unit_string[0]) and not __has_letters(splitted_unit_string[1]):
            # "per 500 gram"
            # "Zak 800 g."
            quantity = Decimal(splitted_unit_string[1])
            unit = splitted_unit_string[2]

        try:
            # 6 x 0.15
            quantity = Decimal(splitted_unit_string[0]) * Decimal(splitted_unit_string[2])
            unit = "liter" if unit_price_description is not None and "liter" in unit_price_description else "kilo"
        except InvalidOperation:
            pass

        if quantity is None and unit is None:
            # "5 x 70gr"
            splitted_value = __split_attached_values(splitted_unit_string[2])
            quantity = Decimal(splitted_unit_string[0]) * Decimal(splitted_value[0].replace(",", "."))
            unit = splitted_value[1]

        if unit is None:
            unit = splitted_unit_string[2]
    elif len(splitted_unit_string) == 4 and unit is None:
        if __has_numbers(splitted_unit_string[0]):
            # "2 x 400 g"
            if __has_letters(splitted_unit_string[2]):
                # "6 x 200g g"
                splitted_unit_string[2] = re.sub(r"\D", "", splitted_unit_string[2])

            if "-" in splitted_unit_string[0]:
                # "6- l -Per zak"
                quantity = int(re.sub(r"\D", "", splitted_unit_string[0]))
                unit = re.sub(r"[0-9]+", "", splitted_unit_string[1])

            if "-" in splitted_unit_string[2]:
                # "3 x 35-50 g."
                new_splitted_unit_string = splitted_unit_string[2].split("-")
                splitted_unit_string[2] = new_splitted_unit_string[0]

            if quantity is None and unit is None:
                quantity = float(splitted_unit_string[0]) * float(splitted_unit_string[2].replace(",", "."))
                unit = splitted_unit_string[3]
        else:
            # "los per 500 g"
            quantity = Decimal(splitted_unit_string[2])
            unit = splitted_unit_string[3]

    if unit is None and quantity is None:
        # "1 m x 6 cm" (e.g. pleisters)
        quantity = Decimal(splitted_unit_string[0])
        unit = splitted_unit_string[1]

    if isinstance(splitted_unit_string[0], str) and splitted_unit_string[0].lower() == "per" and quantity is None:
        # "per pakket"
        quantity = 1
        unit = splitted_unit_string[1]

    if unit == "-pack":
        unit = "piece"

    if "wasbeurten" in unit_string:
        # Defining how much you're buying by indicating how many "wasbeurten" you can get out of 1 bottle...
        # What were they thinking?!
        quantity = 1
        unit = "piece"

    if unit_string == "pet pakket":
        quantity = 6
        unit = "piece"

    if isinstance(quantity, str) and __has_letters(quantity) and __has_numbers(quantity):
        # "150g g"
        quantity = float(re.sub(r"\D", "", quantity))

    if isinstance(quantity, str) and not __has_numbers(quantity):
        # "L l", yup, seriously...
        quantity = 1

    assert quantity is not None
    assert unit is not None and not len(unit) == 0

    return {
        "quantity": float(quantity),
        "unit": unit,
    }
