import asyncio
import logging
from datetime import datetime
from json import JSONEncoder
from typing import Any

from aiohttp import ClientSession
from aiohttp.client_exceptions import ClientResponseError
from aiohttp.http import HTTPStatus

from ..exceptions import StoreFetchError
from ..models import CategoryOptions, NormalizedCategory, NormalizedProduct, Stores
from ..utils import __convert_unit, split_unit_and_quantity

logger = logging.getLogger(__name__)


__DEFAULT_HEADERS = {
    "User-Agent": "android/6.29.3 Model/phone Android/7.0-API24",
    "Host": "api.ah.nl",
    "Cache-Control": "no-cache",
    "Content-Type": "application/json; charset=UTF-8",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept": "*/*",
    "x-application": "AHWEBSHOP",
    "Connection": "keep-alive",
}

__BASE_URL = "https://api.ah.nl"

__UNITS = {
    "stuk": {"unit": "piece", "factor": 1},
    "stuks": {"unit": "piece", "factor": 1},
    "tros": {"unit": "piece", "factor": 1},
    "doos": {"unit": "piece", "factor": 1},
    "krop": {"unit": "piece", "factor": 1},
    "bosje": {"unit": "piece", "factor": 1},
    "bos": {"unit": "piece", "factor": 1},
    "pakket": {"unit": "piece", "factor": 1},
    "st": {"unit": "piece", "factor": 1},
    "bundel": {"unit": "piece", "factor": 1},
    "rol": {"unit": "piece", "factor": 1},
    "rollen": {"unit": "piece", "factor": 1},
    "flessen": {"unit": "piece", "factor": 1},
    "blik": {"unit": "piece", "factor": 1},
    "plakjes": {"unit": "piece", "factor": 1},
    "tabl": {"unit": "piece", "factor": 1},
    "tabletten": {"unit": "piece", "factor": 1},
    "paar": {"unit": "piece", "factor": 1},
    "sachets": {"unit": "piece", "factor": 1},
    "set": {"unit": "piece", "factor": 1},
    "pack": {"unit": "piece", "factor": 1},
    "adviesprijs": {"unit": "piece", "factor": 1},  # e.g. "koffiezetapparaat"
    "lt": {"unit": "ml", "factor": 1000},
    "artikel": {"unit": "piece", "factor": 1},
}


async def __get_access_token(client: ClientSession) -> str:
    json_encoder = JSONEncoder()

    response = await client.post(
        url=f"{__BASE_URL}/mobile-auth/v1/auth/token/anonymous",
        timeout=10,
        headers=__DEFAULT_HEADERS,
        data=json_encoder.encode({"clientId": "appie"}),
    )

    response.raise_for_status()

    json = await response.json()
    return json["access_token"]


async def get_product_by_barcode(
    client: ClientSession,
    barcode: int,
) -> str | None:
    access_token = await __get_access_token(client)

    response = await client.get(
        url=f"{__BASE_URL}/mobile-services/product/search/v1/gtin/{barcode}",
        timeout=10,
        headers={
            **__DEFAULT_HEADERS,
            "Authorization": f"Bearer {access_token}",
        },
    )

    try:
        response.raise_for_status()
    except ClientResponseError as e:
        if e.status == HTTPStatus.NOT_FOUND:
            return None

        raise StoreFetchError(status_code=e.status)

    json = await response.json()
    return str(json["webshopId"])


async def get_data(client: ClientSession) -> dict[str, NormalizedProduct]:
    logger.info("Getting data for the Albert Heijn")
    start_datetime = datetime.now()

    access_token = await __get_access_token(client)

    categories = await __get_categories(client=client, access_token=access_token)

    products: list[NormalizedProduct] = []
    tasks = [__get_products(client=client, access_token=access_token, category=category) for category in categories]
    result = await asyncio.gather(*tasks)

    for category_product_result in result:
        products = products + category_product_result

    filtered_list: dict[str, NormalizedProduct] = {}
    processed_ids = []
    for product in products:
        if product.id not in processed_ids:
            filtered_list[product.id] = product
            processed_ids.append(product.id)

    end_datetime = datetime.now()

    logger.info(
        "Retrieved Albert Heijn data in %s seconds",
        (end_datetime - start_datetime).seconds,
    )

    return filtered_list


async def __get_categories(
    client: ClientSession,
    access_token: str,
) -> list[NormalizedCategory]:
    response = await client.get(
        url=f"{__BASE_URL}/mobile-services/v1/product-shelves/categories",
        timeout=10,
        headers={
            **__DEFAULT_HEADERS,
            "Authorization": f"Bearer {access_token}",
        },
    )

    raw_categories = await response.json()

    categories: list[NormalizedCategory] = []
    for raw_category in raw_categories:
        match raw_category.get("slugifiedName"):
            case "aardappel-groente-fruit":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.FRUIT_AND_VEGETABLES,
                    )
                )
            case "salades-pizza-maaltijden":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.STAPLE_FOOD))
            case "vlees-kip-vis-vega":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.REFRIGERATED_GOOD,
                    )
                )
            case "kaas-vleeswaren-tapas":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.REFRIGERATED_GOOD,
                    )
                )
            case "zuivel-plantaardig-en-eieren":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.REFRIGERATED_GOOD,
                    )
                )
            case "bakkerij-en-banket":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.BREAD_AND_PASTRIES,
                    )
                )
            case "ontbijtgranen-en-beleg":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.REFRIGERATED_GOOD,
                    )
                )
            case "snoep-koek-chips-en-chocolade":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.SWEET_AND_SALTY,
                    )
                )
            case "tussendoortjes":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.SWEET_AND_SALTY,
                    )
                )
            case "frisdrank-sappen-koffie-thee":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.BEVERAGES))
            case "wijn-en-bubbels":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.BEVERAGES))
            case "bier-en-aperitieven":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.BEVERAGES))
            case "pasta-rijst-en-wereldkeuken":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.STAPLE_FOOD))
            case "soepen-sauzen-kruiden-olie":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.STAPLE_FOOD))
            case "sport-en-dieetvoeding":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.STAPLE_FOOD))
            case "diepvries":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.FROZEN))
            case "drogisterij":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.CARE))
            case "baby-en-kind":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.CARE))
            case "huishouden":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.HOUSEHOLD_AND_DOMESTIC_ANIMALS,
                    )
                )
            case "huisdier":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.HOUSEHOLD_AND_DOMESTIC_ANIMALS,
                    )
                )
            case "koken-tafelen-vrije-tijd":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.HOUSEHOLD_AND_DOMESTIC_ANIMALS,
                    )
                )

    return categories


async def __get_products(
    client: ClientSession,
    access_token: str,
    category: NormalizedCategory,
) -> list[NormalizedProduct]:
    ah_items: list[NormalizedProduct] = []

    total_pages = 0
    page = 0
    max_results = 3000

    # An example response from the AH api:
    # {
    #     "webshopId": 579117,
    #     "hqId": 0,
    #     "title": "Unox Good Noodles Kerrie 6-pack",
    #     "salesUnitSize": "6 stuks",
    #     "images": [
    #         {
    #             "width": 800,
    #             "height": 800,
    #             "url": "https://static.ah.nl/dam/product/AHI_4354523130313030333839?revLabel=1&rendition=800x800_JPG_Q90&fileType=binary", # noqa: E501
    #         },
    #         {
    #             "width": 400,
    #             "height": 400,
    #             "url": "https://static.ah.nl/dam/product/AHI_4354523130313030333839?revLabel=1&rendition=400x400_JPG_Q85&fileType=binary", # noqa: E501
    #         },
    #         {
    #             "width": 200,
    #             "height": 200,
    #             "url": "https://static.ah.nl/dam/product/AHI_4354523130313030333839?revLabel=1&rendition=200x200_JPG_Q85&fileType=binary", # noqa: E501
    #         },
    #         {
    #             "width": 48,
    #             "height": 48,
    #             "url": "https://static.ah.nl/dam/product/AHI_4354523130313030333839?revLabel=1&rendition=48x48_GIF&fileType=binary", # noqa: E501
    #         },
    #         {
    #             "width": 80,
    #             "height": 80,
    #             "url": "https://static.ah.nl/dam/product/AHI_4354523130313030333839?revLabel=1&rendition=80x80_JPG&fileType=binary", # noqa: E501
    #         },
    #     ],
    #     "bonusStartDate": "2024-08-02",
    #     "bonusEndDate": "2024-08-04",
    #     "discountType": "AHOO",
    #     "segmentType": "AHOO",
    #     "promotionType": "AHONLINE",
    #     "currentPrice": 2.97,
    #     "priceBeforeBonus": 5.94,
    #     "orderAvailabilityStatus": "IN_ASSORTMENT",
    #     "mainCategory": "Tussendoortjes",
    #     "subCategory": "Noedelsoep",
    #     "brand": "Unox",
    #     "shopType": "AH",
    #     "bonusPeriodDescription": "",
    #     "availableOnline": True,
    #     "isPreviouslyBought": False,
    #     "descriptionHighlights": "<p><ul><li>Unox Good Noodles Kerrie 70G 6x</li></ul></p>",
    #     "propertyIcons": [],
    #     "nix18": False,
    #     "isStapelBonus": False,
    #     "extraDescriptions": [],
    #     "bonusSegmentDescription": "",
    #     "isBonus": True,
    #     "hasListPrice": False,
    #     "descriptionFull": "Dit pakket wordt als losse artikelen geleverd.",
    #     "isOrderable": True,
    #     "isInfiniteBonus": False,
    #     "isSample": False,
    #     "isBonusPrice": True,
    #     "isSponsored": False,
    #     "isVirtualBundle": True,
    #     "virtualBundleItems": [{"productId": 99087, "quantity": 6}],
    #     "multipleItemPromotion": false,
    #     "labelType": "PRODUCT_DISCOUNT_BUNDLE",
    #     "discountLabels": [
    #         {"code": "DISCOUNT_BUNDLE", "defaultDescription": "6 producten", "count": 6}
    #     ],
    # }

    while True:
        response = await client.get(
            url=f"{__BASE_URL}/mobile-services/product/search/v2?page={page}&size=1000&taxonomyId={category.id}",
            timeout=10,
            headers={
                **__DEFAULT_HEADERS,
                "Authorization": f"Bearer {access_token}",
            },
        )

        response.raise_for_status()
        json = await response.json()

        if total_pages == 0:
            total_pages = int(json.get("page", {}).get("totalPages", 1))

        for product in json.get("products", []):
            try:
                unit_string = product.get("salesUnitSize", "1 piece").strip().replace(",", ".")
                unit_price_description = product.get("unitPriceDescription", None)
                unit_and_quantity = split_unit_and_quantity(
                    unit_string=unit_string,
                    unit_price_description=unit_price_description,
                )

                if unit_string == "300 ml PUMP":
                    unit_and_quantity["quantity"] = 1
                    unit_and_quantity["unit"] = "piece"

                converted_unit = __convert_unit(
                    item={
                        "quantity": unit_and_quantity["quantity"],
                        "unit": unit_and_quantity["unit"],
                    },
                    units=__UNITS,
                )

                price = product.get("currentPrice", None)

                if price is None:
                    price = product.get("priceBeforeBonus")

                if price is None:
                    continue  # Don't bother when we don't know any price

                images = product.get("images")

                biggest_image: dict[str, Any] | None = None
                for image in images:
                    if biggest_image is None:
                        biggest_image = image

                    if image["width"] > biggest_image["width"] and image["height"] > biggest_image["height"]:
                        biggest_image = image

                product = NormalizedProduct(
                    store=Stores.ALBERT_HEIJN,
                    id=product.get("webshopId"),
                    name=product.get("title"),
                    price=int(float(price) * 100),  # Convert to cents
                    unit=converted_unit["unit"],
                    shop_unit_str=unit_string,
                    quantity=converted_unit["quantity"],
                    category=category,
                    image_url=biggest_image["url"] if biggest_image is not None else None,
                )
                ah_items.append(product)
            except Exception:
                logger.warning("Failed to parse an AH product: %s", product)

        page += 1

        # Total results we can get is 3000, and "(size * page) + size" must not exceed that number
        # Sadly this means we can never get all the available products
        if page == total_pages or (1000 * page) + 1000 > max_results:
            break

    return ah_items
