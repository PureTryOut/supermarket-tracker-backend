import asyncio
import logging
import math
from datetime import datetime
from typing import Any

from aiohttp import ClientSession
from aiohttp.client_exceptions import ClientResponseError

from ..exceptions import StoreFetchError
from ..models import CategoryOptions, NormalizedCategory, NormalizedProduct, Stores
from ..utils import __convert_unit, split_unit_and_quantity

logger = logging.getLogger(__name__)

__DEFAULT_HEADERS = {
    "User-Agent": "Mozilla/5.0 (Linux; Android 11; M2101K7AG) AppleWebKit/537.36 "
    "(KHTML, like Gecko) Chrome/96.0.4664.45 Mobile Safari/537.36",
    "Content-Type": "application/json",
}

__BASE_URL = "https://mobileapi.jumbo.com"

__UNITS = {
    "pieces": {"unit": "piece", "factor": 1},
    "stuk": {"unit": "piece", "factor": 1},
    "stuks": {"unit": "piece", "factor": 1},
    "doekjes": {"unit": "piece", "factor": 1},
    "rollen": {"unit": "piece", "factor": 1},
    "verbanden": {"unit": "piece", "factor": 1},
    "verbanden+": {"unit": "piece", "factor": 1},
    "huisvuilzakken": {"unit": "piece", "factor": 1},
    "sheets": {"unit": "piece", "factor": 1},
    "nachtverbanden": {"unit": "piece", "factor": 1},
    "rol": {"unit": "piece", "factor": 1},
    "plakken": {"unit": "piece", "factor": 1},
    "inlegkruisjes": {"unit": "piece", "factor": 1},
    "tandenborstel": {"unit": "piece", "factor": 1},
    "eieren": {"unit": "piece", "factor": 1},
    "luiers": {"unit": "piece", "factor": 1},
    "tampons": {"unit": "piece", "factor": 1},
    "wattenschijfjes": {"unit": "piece", "factor": 1},
    "strips": {"unit": "piece", "factor": 1},
    "tandenstokers": {"unit": "piece", "factor": 1},
    "zakjes": {"unit": "piece", "factor": 1},
    "pcs": {"unit": "piece", "factor": 1},
    "inlegger": {"unit": "piece", "factor": 1},
    "ea": {"unit": "piece", "factor": 1},
    "paar": {"unit": "piece", "factor": 1},
    "gebitstabletten": {"unit": "piece", "factor": 1},
    "geurbuiltjes": {"unit": "piece", "factor": 1},
    "flessen": {"unit": "piece", "factor": 1},
    "porties": {"unit": "piece", "factor": 1},
    "vellen": {"unit": "piece", "factor": 1},
    "stokjes": {"unit": "piece", "factor": 1},
    "packs": {"unit": "piece", "factor": 1},
    "plakjes": {"unit": "piece", "factor": 1},
    "grams": {"unit": "g", "factor": 1},
    "GRAM": {"unit": "g", "factor": 1},
    "Gram": {"unit": "g", "factor": 1},
    "G": {"unit": "g", "factor": 1},
    "g.": {"unit": "g", "factor": 1},
    "gr.": {"unit": "g", "factor": 1},
    "grs.": {"unit": "g", "factor": 1},
    "gg": {"unit": "g", "factor": 1},
    "ᵍ": {"unit": "g", "factor": 1},  # Yup, seriously...
    "gms.": {"unit": "g", "factor": 1},
    "KG": {"unit": "g", "factor": 1000},
    "KILO": {"unit": "g", "factor": 1000},
    "kgrams": {"unit": "g", "factor": 1000},
    "L": {"unit": "ml", "factor": 1000},
    "l.": {"unit": "ml", "factor": 1000},
    "Liter": {"unit": "ml", "factor": 1000},
    "litre": {"unit": "ml", "factor": 1000},
    "liters": {"unit": "ml", "factor": 1000},
    "litres": {"unit": "ml", "factor": 1000},
    "сl.": {"unit": "ml", "factor": 10},  # this is not the same as 'cl.' as the c is a different unicode character...
    "cl.": {"unit": "ml", "factor": 10},
    "CL.": {"unit": "ml", "factor": 10},
    "cl": {"unit": "ml", "factor": 10},
    "сl": {"unit": "ml", "factor": 10},  # this is not the same as 'cl' as the c is a different unicode character...
    "mililiters": {"unit": "ml", "factor": 1},
    "ml.": {"unit": "ml", "factor": 1},
    "mlml": {"unit": "ml", "factor": 1},
}


async def get_product_by_barcode(client: ClientSession, barcode: int) -> str | None:
    response = await client.get(
        url=f"{__BASE_URL}/v17/search?q={barcode}",
        timeout=10,
        headers=__DEFAULT_HEADERS,
    )

    json = await response.json()

    try:
        response.raise_for_status()
    except ClientResponseError as e:
        raise StoreFetchError(status_code=e.status)

    if len(json["products"]["data"]) == 0:
        return None

    # The Jumbo never actually finds more than exact matches but since
    # it's using the regular search endpoint it's built to return lists.
    # So if we get here we know we have found only one result
    return json["products"]["data"][0]["id"]


async def get_data(client: ClientSession) -> dict[str, NormalizedProduct]:
    logger.info("Getting data for the Jumbo")
    start_datetime = datetime.now()

    categories = await __get_categories(client=client)

    products: list[NormalizedProduct] = []
    tasks = [__get_products(client=client, category=category) for category in categories]
    result = await asyncio.gather(*tasks)

    for product in result:
        products += product

    product_dict: dict[str, NormalizedProduct] = {}
    for product in products:
        product_dict[product.id] = product

    end_datetime = datetime.now()

    logger.info("Retrieved Jumbo data in %s seconds", (end_datetime - start_datetime).seconds)

    return product_dict


def __split_attached_values(string: str) -> tuple[str, str]:
    # We can't easily use regex as we need to find both floats and whole numbers
    tail = ""
    for i in range(0, len(string)):
        character = string[len(string) - i - 1]
        if character.isdigit():
            # Found first digit from the end
            tail = string[: (len(string) - i)]
            break

    head = string[len(tail) :]
    return tail, head


async def __get_categories(client: ClientSession) -> list[NormalizedCategory]:
    categories: list[NormalizedCategory] = []

    response = await client.get(
        url=f"{__BASE_URL}/v17/categories",
        timeout=10,
        headers=__DEFAULT_HEADERS,
    )

    json = await response.json()

    for raw_category in json.get("categories", {}).get("data", []):
        match raw_category.get("id"):
            case "category:005733":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.FRUIT_AND_VEGETABLES,
                    )
                )
            case "category:005502":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.STAPLE_FOOD))
            case "category:SG2":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.REFRIGERATED_GOOD,
                    )
                )
            case "category:005201":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.REFRIGERATED_GOOD,
                    )
                )
            case "category:SG8":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.REFRIGERATED_GOOD,
                    )
                )
            case "category:005503":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.STAPLE_FOOD))
            case "category:SG5":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.STAPLE_FOOD))
            case "category:SG7":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.BREAD_AND_PASTRIES,
                    )
                )
            case "category:SG9":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.SWEET_AND_SALTY,
                    )
                )
            case "category:SG10":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.BEVERAGES))
            case "category:006152":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.BEVERAGES))
            case "category:SG11":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.BEVERAGES))
            case "category:SG6":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.FROZEN))
            case "category:SG12":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.CARE))
            case "category:SG13":
                categories.append(NormalizedCategory(id=raw_category.get("id"), category=CategoryOptions.CARE))
            case "category:SG14":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.HOUSEHOLD_AND_DOMESTIC_ANIMALS,
                    )
                )

    return categories


async def __get_products(client: ClientSession, category: NormalizedCategory) -> list[NormalizedProduct]:
    jumbo_items: list[NormalizedProduct] = []

    total_pages = 0
    page = 0
    max_size = 30

    # An example response from the Jumbo api.
    # It may optionally include a quantity key.
    # {
    #     "id": "218406STK",
    #     "title": "Jumbo Hondensnack Sushi met Kip 100g",
    #     "quantityOptions": [
    #         {"defaultAmount": 1, "minimumAmount": 1, "amountStep": 1, "unit": "pieces", "maximumAmount": 99}
    #     ],
    #     "available": True,
    #     "productType": "Product",
    #     "crossSellSKUList": [],
    #     "nixProduct": False,
    #     "quantity": "100 g",
    #     "imageInfo": {
    #         "primaryView": [
    #             {
    #                 "url": "https://jumbo.com/dam-images/fit-in/360x360/Products/18092023_1695033525131_1695033539635_8718452325887_1.png", # noqa: E501
    #                 "height": 360,
    #                 "width": 360,
    #             }
    #         ]
    #     },
    #     "prices": {
    #         "price": {"currency": "EUR", "amount": 189},
    #         "unitPrice": {"unit": "kg", "price": {"currency": "EUR", "amount": 1890}},
    #     },
    #     "badgesToDisplay": {},
    #     "sample": False,
    #     "availability": {"sku": "218406STK", "availability": "AVAILABLE"},
    #     "surcharges": [],
    # }

    while True:
        response = await client.get(
            url=f"{__BASE_URL}/v17/search?offset={page * max_size}&limit={max_size}&filters={category.id}",
            timeout=60,
            headers=__DEFAULT_HEADERS,
        )

        try:
            response.raise_for_status()
        except ClientResponseError as e:
            logger.exception(response.text)
            raise StoreFetchError(status_code=e.status)

        json = await response.json()

        if total_pages == 0:
            total_pages = math.ceil(json.get("products", {}).get("total", 30) / max_size)

        for product in json.get("products", {}).get("data", []):
            try:
                unit_string: str | None = product.get("quantity", None)
                unit_and_quantity: dict[str, Any] | None = None

                if unit_string is not None:
                    unit_and_quantity = split_unit_and_quantity(unit_string)

                # Not every product actually seems to include quantity information
                # There is quantityOptions but that seems to mostly default to "1 pieces"
                # Sadly we can't do much more
                if unit_and_quantity is None:
                    unit_and_quantity = {
                        "quantity": product.get("quantityOptions", [])[0].get("defaultAmount"),
                        "unit": product.get("quantityOptions", {})[0].get("unit", {}),
                    }

                converted_unit = __convert_unit(
                    item={
                        "quantity": unit_and_quantity["quantity"],
                        "unit": unit_and_quantity["unit"],
                    },
                    units=__UNITS,
                )

                price: int | None = product.get("prices", {}).get("unitPrice", {}).get("price", {}).get("amount")
                if price is None:
                    continue  # Don't bother when we don't know any price

                images = product.get("imageInfo", {}).get("primaryView", [])

                biggest_image: dict[str, Any] | None = None
                for image in images:
                    if biggest_image is None:
                        biggest_image = image

                    if image["width"] > biggest_image["width"] and image["height"] > biggest_image["height"]:
                        biggest_image = image

                product = NormalizedProduct(
                    store=Stores.JUMBO,
                    id=product.get("id"),
                    name=product.get("title"),
                    price=product.get("prices", {}).get("unitPrice", {}).get("price", {}).get("amount"),
                    unit=converted_unit["unit"],
                    shop_unit_str=unit_string,
                    quantity=converted_unit["quantity"],
                    category=category,
                    image_url=biggest_image["url"] if biggest_image is not None else None,
                )
                jumbo_items.append(product)
            except Exception:
                logger.warning("Failed to parse a Jumbo product: %s", product)

        page += 1

        if page == total_pages:
            break

    return jumbo_items
