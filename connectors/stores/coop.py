import asyncio
import logging
import math
from datetime import datetime

from aiohttp import ClientSession

from ..models import CategoryOptions, NormalizedCategory, NormalizedProduct, Stores
from ..utils import __convert_unit, split_unit_and_quantity

__DEFAULT_HEADERS = {
    "Cache-Control": "no-cache",
    "Content-Type": "application/json",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept": "*/*",
    "Connection": "keep-alive",
}

__BASE_URL = "https://api.coop.nl/INTERSHOP/rest/WFS/COOP-COOPBase-Site/-;loc=nl_NL;cur=EUR/"

__UNITS = {
    "stuk(s)": {"unit": "piece", "factor": 1},
    "zk": {"unit": "piece", "factor": 1},
    "ctl": {"unit": "ml", "factor": 10},
}

logger = logging.getLogger(__name__)


async def get_data(client: ClientSession) -> dict[str, NormalizedProduct]:
    logger.info("Getting data for the COOP")

    start_datetime = datetime.now()

    categories = await __get_categories(client=client)

    products: list[NormalizedProduct] = []
    tasks = [__get_products(client=client, category=category) for category in categories]
    result = await asyncio.gather(*tasks)

    for product in result:
        products += product

    product_dict: dict[str, NormalizedProduct] = {}
    for product in products:
        product_dict[product.id] = product

    end_datetime = datetime.now()

    logger.info("Retrieved COOP data in %s seconds", (end_datetime - start_datetime).seconds)

    return product_dict


async def __get_categories(client: ClientSession) -> list[NormalizedCategory]:
    # Example response:
    # {
    #     "name": "Boodschappen",
    #     "type": "Category",
    #     "attributes": [{"name": "ShowInMenu", "type": "String", "value": "True"}],
    #     "hasOnlineProducts": False,
    #     "hasOnlineSubCategories": True,
    #     "online": "1",
    #     "description": "Nieuwe boodschappen categorie met Dynamic Product Assignment",
    #     "subCategoriesCount": 20,
    #     "categoryPath": [
    #         {
    #             "name": "Boodschappen",
    #             "type": "CategoryPath",
    #             "id": "boodschappen",
    #             "uri": "COOP-COOPBase-Site/-;loc=nl_NL;cur=EUR/categories/boodschappen",
    #         }
    #     ],
    #     "id": "boodschappen",
    #     "categoryRef": "boodschappen@COOP-boodschappen",
    #     "subCategories": [
    #         {
    #             "name": "Seizoen en feestdagen",
    #             "type": "Category",
    #             "hasOnlineSubCategories": True,
    #             "online": "1",
    #             "images": [
    #                 {
    #                     "name": "standaard 1340x1340_local",
    #                     "type": "Image",
    #                     "effectiveUrl": "/INTERSHOP/static/WFS/COOP-COOPBase-Site/-/COOP/nl_NL/catalogpwa/webp/BBQ.webp", # noqa: E501
    #                     "viewID": "standaard",
    #                     "typeID": "1340x1340_local",
    #                     "imageActualHeight": 1340,
    #                     "imageActualWidth": 1340,
    #                     "primaryImage": True,
    #                 }
    #             ],
    #             "categoryPath": [
    #                 {
    #                     "name": "Boodschappen",
    #                     "type": "CategoryPath",
    #                     "id": "boodschappen",
    #                     "uri": "COOP-COOPBase-Site/-;loc=nl_NL;cur=EUR/categories/boodschappen",
    #                 },
    #                 {
    #                     "name": "Seizoen en feestdagen",
    #                     "type": "CategoryPath",
    #                     "id": "seizoen_feestdagen",
    #                     "uri": "COOP-COOPBase-Site/-;loc=nl_NL;cur=EUR/categories/boodschappen/seizoen_feestdagen",
    #                 },
    #             ],
    #             "id": "seizoen_feestdagen",
    #             "categoryRef": "seizoen_feestdagen@COOP-boodschappen",
    #             "uri": "COOP-COOPBase-Site/-;loc=nl_NL;cur=EUR/categories/boodschappen/seizoen_feestdagen",
    #         },
    #     ],
    # }

    # The coop has more categories than just groceries, but that's all we care for for now
    response = await client.get(
        url=f"{__BASE_URL}/categories/boodschappen",
        timeout=10,
        headers=__DEFAULT_HEADERS,
    )

    response.raise_for_status()

    json = await response.json()
    raw_categories = json["subCategories"]

    categories: list[NormalizedCategory] = []
    for raw_category in raw_categories:
        match raw_category.get("name"):
            case "Seizoen en feestdagen":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.UNKNOWN,
                    )
                )
            case "Fruit":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.FRUIT_AND_VEGETABLES,
                    )
                )
            case "Groenten":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.FRUIT_AND_VEGETABLES,
                    )
                )
            case " Aardappelen, pasta en rijst":  # Space prefix is not a typo
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.STAPLE_FOOD,
                    )
                )
            case "Vlees, kip, vis en vegetarisch":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.REFRIGERATED_GOOD,
                    )
                )
            case "Maaltijden, soepen en salades":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.STAPLE_FOOD,
                    )
                )
            case "Koken, sauzen en smaakmakers":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.STAPLE_FOOD,
                    )
                )
            case "Diepvries":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.FROZEN,
                    )
                )
            case "Brood, bakkerij en ontbijtgranen":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.BREAD_AND_PASTRIES,
                    )
                )
            case "Broodbeleg, kaas en vleeswaren":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.REFRIGERATED_GOOD,
                    )
                )
            case "Zuivel, eieren en boter":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.REFRIGERATED_GOOD,
                    )
                )
            case "Koek en snoep":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.SWEET_AND_SALTY,
                    )
                )
            case "Koffie en thee":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.BEVERAGES,
                    )
                )
            case "Frisdrank, sappen en water":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.BEVERAGES,
                    )
                )
            case "Wijn, bier en sterke drank":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.BEVERAGES,
                    )
                )
            case "Chips, nootjes en borrelhapjes":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.SWEET_AND_SALTY,
                    )
                )
            case "Huisdierproducten":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.HOUSEHOLD_AND_DOMESTIC_ANIMALS,
                    )
                )
            case "Baby, verzorging en hygiëne":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.CARE,
                    )
                )
            case "Huishouden en non-food":
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.HOUSEHOLD_AND_DOMESTIC_ANIMALS,
                    )
                )
            case " Bewuste voeding":  # Space prefix is not a typo
                categories.append(
                    NormalizedCategory(
                        id=raw_category.get("id"),
                        category=CategoryOptions.STAPLE_FOOD,
                    )
                )
    return categories


async def __get_products(
    client: ClientSession,
    category: NormalizedCategory,
) -> list[NormalizedProduct]:
    coop_items: list[NormalizedProduct] = []

    total_pages = 0
    page = 0
    max_size = 20

    # Example response:
    # {
    #     "total": 811,
    #     "offset": 0,
    #     "amount": 15,
    #     "elements": [
    #         {
    #             "attributes": [
    #                 {"name": "sku", "type": "String", "value": "8718989006495"},
    #                 {
    #                     "name": "minOrderQuantity",
    #                     "type": "ResourceAttribute",
    #                     "value": {"type": "Quantity", "value": 1, "unit": "pcs"},
    #                 },
    #                 {"name": "packingUnit", "type": "String", "value": "pcs"},
    #                 {"name": "productMaster", "type": "Boolean", "value": False},
    #                 {"name": "mastered", "type": "Boolean", "value": False},
    #                 {"name": "availability", "type": "Boolean", "value": True},
    #                 {"name": "manufacturer", "type": "String", "value": "Refresco B.V."},
    #                 {"name": "inStock", "type": "Boolean", "value": True},
    #                 {
    #                     "name": "listPrice",
    #                     "type": "ResourceAttribute",
    #                     "value": {"type": "Money", "value": 17.99, "currencyMnemonic": "EUR", "currency": "EUR"},
    #                 },
    #                 {
    #                     "name": "salePrice",
    #                     "type": "ResourceAttribute",
    #                     "value": {"type": "Money", "value": 17.99, "currencyMnemonic": "EUR", "currency": "EUR"},
    #                 },
    #                 {
    #                     "name": "image",
    #                     "type": "String",
    #                     "value": "https://syndy-content.azureedge.net/media/products/58260000-ff27-0003-66f9-08db2eb4034f/images/AW0HA3GItENDpZ-NPK0ohvI./AAAAVow7N25nzgjb5QhjuA.290x290.jpg", # noqa: E501
    #                 },
    #                 {"name": "roundedAverageRating", "type": "String", "value": "0.0"},
    #             ],
    #             "uri": "COOP-COOPBase-Site/-;loc=nl_NL;cur=EUR/categories/boodschappen/frisdrank_sappen_en_water/products/8718989006495", # noqa: E501
    #             "title": "Slammers Energy drink 24x250 ml",
    #             "description": "Slammers Energy drink",
    #             "attributeGroup": {
    #                 "name": "PRODUCT_LIST_DETAIL_ATTRIBUTES",
    #                 "attributes": [
    #                     {"name": "titelManueel", "type": "String", "value": "Slammers Energy drink 24x250 ml"},
    #                     {"name": "Inhoud", "type": "String", "value": "250 ml"},
    #                     {
    #                         "name": "syndy nutrients link",
    #                         "type": "String",
    #                         "value": "58260000-ff27-0003-66f9-08db2eb4034f",
    #                     },
    #                     {"name": "Longtail", "type": "Boolean", "value": False},
    #                 ],
    #                 "displayName": "Product List Detail Attributes",
    #             },
    #         },
    #     ],
    # }

    # Calling the COOP API sometimes times out, we'll try it 3 times
    tries = 0

    while True:
        try:
            response = await client.get(
                url=f"{__BASE_URL}/categories/boodschappen/{category.id}/products"
                f"?offset={page * max_size}&amount={max_size}&attributeGroup=PRODUCT_LIST_DETAIL_ATTRIBUTES"
                "&attrs=sku,salePrice,listPrice,availability,manufacturer,image,minOrderQuantity,inStock,promotions,"
                "packingUnit,mastered,productMaster,productMasterSKU,roundedAverageRating,longtail,sticker,maxXLabel,"
                "Inhoud,SUPprice",
                timeout=10,
                headers=__DEFAULT_HEADERS,
            )

            tries = 0

            response.raise_for_status()
            json = await response.json()

            if total_pages == 0:
                total_pages = math.ceil(json.get("total") / max_size)

            for product in json.get("elements", []):
                try:
                    unit_string: str | None = None
                    for attribute in product["attributeGroup"]["attributes"]:
                        if attribute["name"] != "Inhoud":
                            continue

                        unit_string = attribute["value"]

                    if unit_string is None:
                        logger.warning("The COOP product %s seems to have no unit string defined", product["title"])
                        continue

                    unit_and_quantity = split_unit_and_quantity(unit_string)
                    try:
                        converted_unit = __convert_unit(
                            item={
                                "quantity": unit_and_quantity["quantity"],
                                "unit": unit_and_quantity["unit"],
                            },
                            units=__UNITS,
                        )
                    except KeyError:
                        logger.exception(product)

                    product_id: str | None = None
                    price: str | None = None
                    image_url: str | None = None

                    for attribute in product["attributes"]:
                        if attribute["name"] == "sku":
                            product_id = attribute["value"]

                        if attribute["name"] == "salePrice":
                            price = attribute["value"]["value"]

                        if attribute["name"] == "image":
                            image_url = attribute["value"]

                    if product_id is None:
                        logger.warning("The COOP product %s seems to have no store ID defined", product["title"])
                        continue

                    if price is None:
                        continue  # Don't bother when we don't know any price

                    product = NormalizedProduct(
                        store=Stores.COOP,
                        id=product_id,
                        ean_code=product_id,
                        name=product.get("title"),
                        price=int(float(price) * 100),  # Convert to cents
                        unit=converted_unit["unit"],
                        shop_unit_str=unit_string,
                        quantity=converted_unit["quantity"],
                        category=category,
                        image_url=image_url,
                    )
                    coop_items.append(product)
                except Exception:
                    logger.warning("Failed to parse a COOP product: %s", product)

            page += 1

            # Total results we can get is 3000, and "(size * page) + size" must not exceed that number
            # Sadly this means we can never get all the available products
            if page == total_pages:
                break
        except asyncio.TimeoutError:
            tries += 1

            if tries == 3:
                # Let's give up
                break

    return coop_items
