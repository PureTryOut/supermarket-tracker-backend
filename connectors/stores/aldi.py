#!/usr/bin/env python3

import asyncio
import logging
import re
from datetime import datetime

from aiohttp import ClientResponse, ClientSession

from ..models import CategoryOptions, NormalizedCategory, NormalizedProduct, Stores
from ..utils import __convert_unit, split_unit_and_quantity

logger = logging.getLogger(__name__)

__DEFAULT_HEADERS = {
    "User-Agent": "okhttp/3.9.0",
    "Content-Type": "application/json",
}

__BASE_URL = "https://webservice.aldi.nl/api/v1"

__UNITS = {
    "g.": {"unit": "gram", "factor": 1},
    "-g": {"unit": "gram", "factor": 1},
    "-g-per": {"unit": "gram", "factor": 1},  # 2x350-g-Per
    "-nullg": {"unit": "gram", "factor": 1},  # 225-nullg
    "mg": {"unit": "gram", "factor": 0.001},
    "-kg": {"unit": "gram", "factor": 1000},
    "kg.": {"unit": "gram", "factor": 1000},
    "-ml": {"unit": "ml", "factor": 1},
    "ml.": {"unit": "ml", "factor": 1},
    "ml-l-2.89": {"unit": "ml", "factor": 1},  # 750 ml-l-2.89, seems to include price?!
    "cl.": {"unit": "ml", "factor": 10},
    "-l": {"unit": "ml", "factor": 1000},
    "l.": {"unit": "ml", "factor": 1000},
    "l.-l": {"unit": "ml", "factor": 1000},
    "zak": {"unit": "piece", "factor": 1},
    "st.": {"unit": "piece", "factor": 1},
    "stuk": {"unit": "piece", "factor": 1},
    "stuk.": {"unit": "piece", "factor": 1},
    "stuks": {"unit": "piece", "factor": 1},
    "-stuks": {"unit": "piece", "factor": 1},
    "stuks.": {"unit": "piece", "factor": 1},
    "per": {"unit": "piece", "factor": 1},  # 400-Per zak
    "heel": {"unit": "piece", "factor": 1},
    "half": {"unit": "piece", "factor": 0.5},
    "vaks.": {"unit": "piece", "factor": 1},
    "zakjes": {"unit": "piece", "factor": 1},
    "zakjes.": {"unit": "piece", "factor": 1},
    "vellen": {"unit": "piece", "factor": 1},
    "rollen": {"unit": "piece", "factor": 1},
    "pak": {"unit": "piece", "factor": 1},
    "-pack.": {"unit": "piece", "factor": 1},
    "tabletten.": {"unit": "piece", "factor": 1},
    "bos": {"unit": "piece", "factor": 1},
    "tros": {"unit": "piece", "factor": 1},
    "kistje": {"unit": "piece", "factor": 1},
    "fles": {"unit": "piece", "factor": 1},  # TODO: 700-Per fles, need to parse this into liters somehow
}


async def get_data(client: ClientSession) -> dict[str, NormalizedProduct]:
    logger.info("Getting data for the Aldi")
    start_datetime = datetime.now()

    categories = await __get_categories(client=client)

    result = []
    tasks = [__get_products(client=client, category=category) for category in categories]
    result = await asyncio.gather(*tasks)

    products: list[NormalizedProduct] = []
    for category_product_result in result:
        products = products + category_product_result

    filtered_list: dict[str, NormalizedProduct] = {}
    processed_ids = []
    for product in products:
        if product.id not in processed_ids:
            filtered_list[product.id] = product
            processed_ids.append(product.id)

    end_datetime = datetime.now()

    logger.info(
        "Retrieved Aldi data in %s seconds",
        (end_datetime - start_datetime).seconds,
    )

    return filtered_list


async def __get_categories(client: ClientSession) -> list[NormalizedCategory]:
    response: ClientResponse = await client.get(
        url=f"{__BASE_URL}/products.json",
        # url=f"{BASE_URL}/productCollections.json",
        timeout=10,
        headers=__DEFAULT_HEADERS,
    )

    # An example response from the Aldi api:
    # {
    #     "productCollections": [
    #         {
    #             "hasProducts": True,
    #             "id": "trotsvanaldi1",
    #             "shortTitle": "Winnaars",
    #             "teaserImage": {
    #                 "alt": "Winnaars",
    #                 "altText": "Winnaars",
    #                 "author": "",
    #                 "baseUrl": "https://www.aldi.nl/content/dam/aldi/netherlands/winnaars/8789_Teaser_2880x2160_TvA_groep21png",
    #                 "imageLastPublished": 1690465944069,
    #                 "location": "",
    #                 "renditionsSuffixes": {
    #                     "288": "/jcr:content/renditions/original.transform/288w/img.1690380888099.png",
    #                     "440": "/jcr:content/renditions/original.transform/440w/img.1690380888099.png",
    #                     "592": "/jcr:content/renditions/original.transform/592w/img.1690380888099.png",
    #                     "736": "/jcr:content/renditions/original.transform/736w/img.1690380888099.png",
    #                     "900": "/jcr:content/renditions/original.transform/900w/img.1690380888099.png",
    #                     "1250": "/jcr:content/renditions/original.transform/1250w/img.1690380888099.png",
    #                     "1817": "/jcr:content/renditions/original.transform/1817w/img.1690380888099.png",
    #                     "2800": "/jcr:content/renditions/original.transform/2800w/img.1690380888099.png",
    #                 },
    #                 "title": "",
    #                 "type": "image",
    #                 "valid": True,
    #             },
    #             "title": "Winnaars",
    #         },
    #         {
    #             "hasProducts": False,
    #             "id": "aardappels-groente-fruit",
    #             "shortTitle": "Aardappels, groente, fruit",
    #             "teaserImage": {
    #                 "alt": "Aardappels, groente, fruit van ALDI",
    #                 "altText": "Aardappels, groente, fruit van ALDI",
    #                 "author": "",
    #                 "baseUrl": "https://www.aldi.nl/content/dam/aldi/netherlands/products/teaser-096-Aardappels-groente-fruit.png",
    #                 "imageLastPublished": 1704884715188,
    #                 "location": "",
    #                 "renditionsSuffixes": {
    #                     "288": "/jcr:content/renditions/opt.288w.png.res/1704884639555/opt.288w.png",
    #                     "440": "/jcr:content/renditions/opt.440w.png.res/1704884639555/opt.440w.png",
    #                     "592": "/jcr:content/renditions/opt.592w.png.res/1704884639555/opt.592w.png",
    #                     "736": "/jcr:content/renditions/opt.736w.png.res/1704884639555/opt.736w.png",
    #                     "900": "/jcr:content/renditions/opt.900w.png.res/1704884639555/opt.900w.png",
    #                     "1250": "/jcr:content/renditions/opt.1250w.png.res/1704884639555/opt.1250w.png",
    #                     "1817": "/jcr:content/renditions/opt.1817w.png.res/1704884639555/opt.1817w.png",
    #                     "2800": "/jcr:content/renditions/opt.2800w.png.res/1704884639555/opt.2800w.png",
    #                 },
    #                 "title": "",
    #                 "type": "image",
    #                 "valid": True,
    #             },
    #             "title": "Aardappels, groente, fruit",
    #         },
    #     ],
    # }

    response.raise_for_status()
    raw_categories = await response.json()

    categories: list[NormalizedCategory] = []
    for raw_category in raw_categories["productCollections"]:
        main_category = raw_category.get("id").split("/")[0]

        # When a category has subcategories it often itself doesn't have products.
        # In that case we don't need to bother storing it either.
        if raw_category.get("hasProducts") is False:
            continue

        match main_category:
            case "aardappels-groente-fruit":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.FRUIT_AND_VEGETABLES,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "brood-bakkerij":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.BREAD_AND_PASTRIES,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "ontbijtgranen-broodbeleg-tussendoortjes":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.REFRIGERATED_GOOD,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "zuivel-eieren-boter":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.REFRIGERATED_GOOD,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "kaas-vleeswaren-tapas":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.REFRIGERATED_GOOD,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "wijn":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.BEVERAGES,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "vlees-vis-vega":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.REFRIGERATED_GOOD,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "maaltijden-salades":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.STAPLE_FOOD,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "pasta-rijst-bakken-internationale-keuken":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.STAPLE_FOOD,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "soepen-sauzen-smaakmakers-conserven":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.STAPLE_FOOD,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "snoep-koeken":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.SWEET_AND_SALTY,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "chips-noten":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.SWEET_AND_SALTY,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "diepvries":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.FROZEN,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "bier-en-likeuren":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.BEVERAGES,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "sappen-frisdrank":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.BEVERAGES,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "thee-koffie":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.BEVERAGES,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "huishouden":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.HOUSEHOLD_AND_DOMESTIC_ANIMALS,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "baby-persoonlijke-verzorging":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.CARE,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "huisdieren":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.HOUSEHOLD_AND_DOMESTIC_ANIMALS,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "trotsvanaldi1":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.UNKNOWN,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "sintassortiment":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.UNKNOWN,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "najaar":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.UNKNOWN,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "cadeaukaarten":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.UNKNOWN,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "feestassortiment":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.UNKNOWN,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "paaschocolade":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.SWEET_AND_SALTY,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "BBQ":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.UNKNOWN,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "zonnebrand":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.CARE,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "dranken":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.BEVERAGES,
                        shop_id=raw_category.get("id"),
                    ),
                )
            case "Herfst_assortiment":
                categories.append(
                    NormalizedCategory(
                        id=main_category,
                        category=CategoryOptions.UNKNOWN,
                        shop_id=raw_category.get("id"),
                    ),
                )

    return categories


async def __get_products(
    client: ClientSession,
    category: NormalizedCategory,
) -> list[NormalizedProduct]:
    products: list[NormalizedProduct] = []

    response: ClientResponse = await client.get(
        # url=f"{BASE_URL}/products/{category.id}.json",
        url=f"{__BASE_URL}/products/{category.shop_id}.json",
        timeout=10,
        headers=__DEFAULT_HEADERS,
    )

    # Example Aldi API response:
    # {
    #     "articleGroups": [
    #         {
    #             "anchorId": "aardappeken",
    #             "articles": [
    #                 {
    #                     "additionalInfoUrl": "",
    #                     "allBadgeImages": [
    #                         {
    #                             "alt": "Planet proof logo",
    #                             "altText": "",
    #                             "author": "",
    #                             "baseUrl": "https://www.aldi.nl/content/dam/aldi/netherlands/logos/Planet_proof_logo_RGB.png",
    #                             "imageLastPublished": 1583337694844,
    #                             "location": "",
    #                             "renditionsSuffixes": {
    #                                 "1": "/jcr:content/renditions/original.transform/1x/img.1582900324435.png",
    #                                 "2": "/jcr:content/renditions/original.transform/2x/img.1582900324435.png",
    #                             },
    #                             "title": "",
    #                             "type": "image",
    #                             "valid": True,
    #                         },
    #                         {
    #                             "alt": "Kwaliteit",
    #                             "altText": "",
    #                             "author": "",
    #                             "baseUrl": "https://www.aldi.nl/content/dam/aldi/netherlands/offers/woensdag/2023/wk09/Kwaliteit-uit-Nederland_logo.png",
    #                             "imageLastPublished": 1701960665745,
    #                             "location": "",
    #                             "renditionsSuffixes": {
    #                                 "1": "/jcr:content/renditions/original.transform/1x/img.1701947860646.png",
    #                                 "2": "/jcr:content/renditions/original.transform/2x/img.1701947860646.png",
    #                             },
    #                             "title": "",
    #                             "type": "image",
    #                             "valid": True,
    #                         },
    #                     ],
    #                     "ambientImage": {
    #                         "alt": "",
    #                         "altText": "",
    #                         "author": "",
    #                         "baseUrl": "None",
    #                         "imageLastPublished": 0,
    #                         "location": "",
    #                         "renditionsSuffixes": {},
    #                         "title": "",
    #                         "type": "image",
    #                         "valid": False,
    #                     },
    #                     "articleFlags": [],
    #                     "articleFlagsConfig": {
    #                         "availability": "",
    #                         "marketingText": "",
    #                         "productRecall": False,
    #                         "recommendation": False,
    #                     },
    #                     "articleFlagsLabels": {
    #                         "biocidalProductWarning": "None",
    #                         "notAvailable": "None",
    #                         "online": "None",
    #                         "onlineAndStore": "None",
    #                         "productRecall": "None",
    #                         "recommendation": "None",
    #                         "soldOut": "None",
    #                         "soldOutOnline": "None",
    #                     },
    #                     "articleGroupModel": "None",
    #                     "articleId": "products/aardappels-groente-fruit/aardappelen/2010640-1-0",
    #                     "articleNumber": "2010640-1-0",
    #                     "articleTileImageUsed": False,
    #                     "badgeImage": {
    #                         "alt": "Planet proof logo",
    #                         "altText": "",
    #                         "author": "",
    #                         "baseUrl": "https://www.aldi.nl/content/dam/aldi/netherlands/logos/Planet_proof_logo_RGB.png",
    #                         "imageLastPublished": 1583337694844,
    #                         "location": "",
    #                         "renditionsSuffixes": {
    #                             "1": "/jcr:content/renditions/original.transform/1x/img.1582900324435.png",
    #                             "2": "/jcr:content/renditions/original.transform/2x/img.1582900324435.png",
    #                         },
    #                         "title": "",
    #                         "type": "image",
    #                         "valid": True,
    #                     },
    #                     "basePriceFormatted": "",
    #                     "basePriceValue": "None",
    #                     "brandAlternative": "None",
    #                     "brandName": "",
    #                     "certificates": [],
    #                     "colorSchemeClass": "ct-o-g",
    #                     "currencySymbol": "None",
    #                     "energyEfficiencyClass": "None",
    #                     "energyEfficiencyClassHtml": "None",
    #                     "energyEfficiencyColor": "None",
    #                     "energyEfficiencyIndex": 0,
    #                     "energyEfficiencyNewLayout": False,
    #                     "energyEfficiencyScale": "None",
    #                     "filterValues": {},
    #                     "footnotes": "",
    #                     "hasPricePerVariant": False,
    #                     "hasVariants": False,
    #                     "homeBrand": False,
    #                     "isABrand": False,
    #                     "isBasePriceSimple": True,
    #                     "isBiocidalProduct": False,
    #                     "isDrainedWeight": False,
    #                     "isExternalArticle": False,
    #                     "isNotAvailable": False,
    #                     "isOldPriceMsrp": False,
    #                     "isSoldOut": False,
    #                     "isTipp": False,
    #                     "kvNumber": "2010640",
    #                     "longDescription": "",
    #                     "notAvailableText": "None",
    #                     "oldPrice": "None",
    #                     "oldPriceFormatted": "None",
    #                     "oldPricePrefix": "None",
    #                     "price": "1.49",
    #                     "priceFormatted": "1.49",
    #                     "priceInfo": "",
    #                     "priceReduction": "",
    #                     "primaryImage": {
    #                         "alt": "Culinaire smaak aardappels van ALDI",
    #                         "altText": "",
    #                         "author": "",
    #                         "baseUrl": "https://www.aldi.nl/content/dam/aldi/netherlands/offers/weekactie/2024/wk19/2010640_01.png",
    #                         "imageLastPublished": 1714381950118,
    #                         "location": "",
    #                         "renditionsSuffixes": {
    #                             "288": "/jcr:content/renditions/opt.288w.png.res/1714111561986/opt.288w.png",
    #                             "440": "/jcr:content/renditions/opt.440w.png.res/1714111561986/opt.440w.png",
    #                             "592": "/jcr:content/renditions/opt.592w.png.res/1714111561986/opt.592w.png",
    #                             "736": "/jcr:content/renditions/opt.736w.png.res/1714111561986/opt.736w.png",
    #                             "900": "/jcr:content/renditions/opt.900w.png.res/1714111561986/opt.900w.png",
    #                             "1250": "/jcr:content/renditions/opt.1250w.png.res/1714111561986/opt.1250w.png",
    #                             "1817": "/jcr:content/renditions/opt.1817w.png.res/1714111561986/opt.1817w.png",
    #                             "2800": "/jcr:content/renditions/opt.2800w.png.res/1714111561986/opt.2800w.png",
    #                         },
    #                         "title": "",
    #                         "type": "image",
    #                         "valid": True,
    #                     },
    #                     "salesUnit": "Zak 800 g.",
    #                     "shoppingListTileUrl": "https://www.aldi.nl/producten/aardappels-groente-fruit/aardappelen/snippet-2010640-1-0.shoppinglisttile.article.html",
    #                     "shortDescription": "   ",
    #                     "showAvailability": False,
    #                     "showNotAvailableHint": False,
    #                     "showPrice": True,
    #                     "showPriceInfo": False,
    #                     "showQuantities": True,
    #                     "showReminder": False,
    #                     "soldOutText": "None",
    #                     "title": "Culinaire smaak aardappels",
    #                     "warrantyLogo": "None",
    #                     "warrantyProducerLogo": "None",
    #                     "webDetailURL": "https://www.aldi.nl/producten/aardappels-groente-fruit/aardappelen/culinaire-smaak-aardappels-2010640-1-0.article.html",
    #                 },
    #             ],
    #         },
    #     ],
    # }

    response.raise_for_status()
    raw_products = await response.json()

    for i, value in enumerate(raw_products["articleGroups"]):
        for raw_product in raw_products["articleGroups"][i]["articles"]:
            unit_string = raw_product.get("salesUnit", "1 piece").strip()

            if len(unit_string) == 0:
                # Utterly useless...
                continue

            if unit_string == "kg-5.90":
                # Only has info on price per kg, but doesn't sell per kg
                # There actually is no valid unit in this info
                # articleId "products/snoep-koeken/koek/9063-1-0"
                continue

            # Remove any duplicated space
            unit_string = re.sub(" +", " ", unit_string)
            unit_and_quantity = split_unit_and_quantity(unit_string=unit_string)

            converted_unit = __convert_unit(
                item={
                    "quantity": unit_and_quantity["quantity"],
                    "unit": unit_and_quantity["unit"],
                },
                units=__UNITS,
            )

            price = raw_product.get("priceFormatted", None)

            if price is None:
                price = raw_product.get("price")

            if price is None:
                continue  # Don't bother when we don't know any price

            product = NormalizedProduct(
                store=Stores.ALDI,
                id=raw_product.get("articleNumber"),
                name=raw_product.get("title"),
                price=int(float(price) * 100),  # Convert to cents
                unit=converted_unit["unit"],
                shop_unit_str=unit_string,
                quantity=converted_unit["quantity"],
                category=category,
                image_url=raw_product.get("primaryImage").get("baseUrl"),
            )
            products.append(product)

    return products
