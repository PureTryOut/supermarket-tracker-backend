# Generated by Django 5.0.7 on 2024-08-22 19:38

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("supermarket_tracker_backend", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="product",
            name="store",
            field=models.CharField(
                choices=[
                    ("ALBERT_HEIJN", "Albert Heijn"),
                    ("JUMBO", "Jumbo"),
                    ("COOP", "COOP"),
                    ("ALDI", "Aldi"),
                ],
                help_text="Which store the product comes from",
                max_length=20,
            ),
        ),
    ]
