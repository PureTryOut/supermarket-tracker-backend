import asyncio
import logging
from typing import Coroutine

from aiohttp import ClientResponseError, ClientSession
from django.db.models import QuerySet
from django.http import FileResponse, HttpResponse
from django.shortcuts import aget_object_or_404, get_object_or_404
from django.urls import reverse
from django.views import View
from django_filters import rest_framework as filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import serializers, status
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework.permissions import AllowAny

from connectors.exceptions import StoreFetchError
from connectors.stores import ah, jumbo

from ..models import Product, ProductMistake, ProductPrice, StoreChoices

logger = logging.getLogger(__name__)


class _ProductFilterSet(filters.FilterSet):
    product_id = filters.CharFilter(label="Filter/find products by their store's product ID.")
    ean_code = filters.NumberFilter(
        label="Filter/find products by EAN code. Note that since EAN codes are user-submitted a lot of products "
        "won't have these available yet.",
        method="get_ean_code",
    )
    store = filters.ChoiceFilter(choices=StoreChoices.choices)

    def get_ean_code(self, queryset: QuerySet, key: str, value: int):
        # We first query our own database.
        # If we find that the product doesn't exist in our knowledge of a store's database
        # we query the store itself.
        # If we find a result there we connect the EAN code to the product in our own database so
        # we don't have to query that store again for this product in the future.
        not_found_in_stores: list[str] = []
        for store in StoreChoices.choices:
            # The COOP already stores their products with EAN codes so if we don't have it our DB already
            # they don't have it
            if store[0] == StoreChoices.COOP:
                continue

            exists_in_store = queryset.filter(store=store[0], ean_code=value).exists()

            if not exists_in_store:
                not_found_in_stores.append(store[0])

        async def get_product(stores: list[str]):
            async with ClientSession() as client:
                tasks: list[Coroutine] = []
                for store in not_found_in_stores:
                    match store:
                        case StoreChoices.ALBERT_HEIJN:
                            tasks += [ah.get_product_by_barcode(client, value)]
                        case StoreChoices.JUMBO:
                            tasks += [jumbo.get_product_by_barcode(client, value)]

                return await asyncio.gather(*tasks, return_exceptions=True)

        if len(not_found_in_stores) > 0:
            result = asyncio.run(get_product(not_found_in_stores))

        for index, store in enumerate(not_found_in_stores):
            if index > len(result):
                # Not every store we support has an EAN code API we can use
                break

            if isinstance(result[index], StoreFetchError):
                logger.error(f"Error while fetching product from {store} by barcode {value}: {result.status_code}")
                continue

            if result[index] is None:
                continue

            store_product: Product | None = Product.objects.filter(
                product_id=result[index],
                store=store,
            ).first()

            if store_product is None:
                # TODO: the store clearly knows about this product so we should too, handle fetching that product
                continue

            store_product.ean_code = value
            store_product.save()

        return queryset.filter(ean_code=value)


class _ProductSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = [
            "product_id",
            "name",
            "store",
            "unit",
            "quantity",
            "shop_unit_string",
            "category",
            "ean_code",
            "image",
        ]

    def get_image(self, obj: Product) -> str | None:
        if obj.image_url is None:
            return None

        request = self.context["request"]
        return request.build_absolute_uri(
            reverse(
                "product-image",
                kwargs={"product_id": obj.pk},
            ),
        )


class ListProductsAPIView(ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = _ProductSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filterset_class = _ProductFilterSet
    search_fields = ["name"]
    queryset = Product.objects.all().order_by("product_id")


class _ProductPriceFilterSet(filters.FilterSet):
    date = filters.DateFilter()

    class Meta:
        fields = {
            "price": ["gt"],
        }


class _ProductPriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductPrice
        fields = [
            "date",
            "price",
        ]


class ListProductPricesAPIView(ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = _ProductPriceSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = _ProductPriceFilterSet

    def get_queryset(self):
        product_id: int = self.request.parser_context["kwargs"]["product_id"]
        return ProductPrice.objects.filter(product__product_id=product_id).order_by("product_id")


class _ProductMistakeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductMistake
        fields = [
            "message",
            "email",
        ]

    def create(self, validated_data):
        product_id: str = self.context["request"].parser_context["kwargs"]["product_id"]
        validated_data["product"] = get_object_or_404(Product, product_id=product_id)

        return super().create(validated_data)


class SubmitProductMistakeAPIView(CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = _ProductMistakeSerializer
    queryset = ProductMistake.objects.all().order_by("product_id")


class ProductImageAPIView(View):
    async def get(self, request, product_id, *args, **kwargs):
        product: Product = await aget_object_or_404(Product, pk=product_id)

        async with ClientSession() as client:
            try:
                image = await product.get_image(client)

                if image is not None:
                    response = FileResponse(image)
                    response["Content-Length"] = image.file.size
                    return response
            except ClientResponseError as exc:
                if exc.status == status.HTTP_403_FORBIDDEN:
                    return HttpResponse(status=status.HTTP_502_BAD_GATEWAY)
                else:
                    raise exc

            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
