import asyncio
import logging
from datetime import date
from typing import Coroutine

from aiohttp import ClientSession
from asgiref.sync import async_to_sync
from celery import shared_task

from connectors import stores
from connectors.models import NormalizedProduct, Stores
from supermarket_tracker_backend.models import Product, ProductPrice

logger = logging.getLogger(__name__)


@shared_task
def fetch_store_data(store: str):
    async_to_sync(fetch_data)(Stores(store))


async def fetch_data(store: Stores):
    async def insert_into_database(product: NormalizedProduct):
        # Sadly we can't use abulk_create, which would save a lot of time, because of having to create 2
        # separate models and dealing with conflicts in the database at the same time
        defaults = {
            "name": product.name.strip(),
            "unit": product.unit,
            "quantity": product.quantity,
            "category": product.category.category.value,
            "shop_unit_string": product.shop_unit_str,
        }

        if product.ean_code is not None:
            # Some stores store EAN codes themselves
            defaults = {
                **defaults,
                "ean_code": product.ean_code,
            }

        if product.image_url is not None:
            defaults = {
                **defaults,
                "image_url": product.image_url,
            }

        saved_product, _ = await Product.objects.aupdate_or_create(
            product_id=product.id,
            store=product.store.value,
            defaults=defaults,
        )

        latest_price: ProductPrice | None = await (
            ProductPrice.objects.filter(product=saved_product).order_by("-date").afirst()
        )

        today: date = date.today()
        # Make sure to only add a price entry if it's different from the last entry
        if latest_price is None or (latest_price.date != today and latest_price.price != product.price):
            await ProductPrice.objects.acreate(
                product=saved_product,
                date=today,
                price=product.price,
            )

    async with ClientSession() as client:
        match store:
            case Stores.ALBERT_HEIJN:
                result = await stores.ah.get_data(client)
            case Stores.ALDI:
                result = await stores.aldi.get_data(client)
            case Stores.COOP:
                result = await stores.coop.get_data(client)
            case Stores.JUMBO:
                result = await stores.jumbo.get_data(client)

        tasks: list[Coroutine] = []
        for product in result.values():
            tasks += [insert_into_database(product)]

        logger.info("Data retrieved. Saving products to the database")
        await asyncio.gather(*tasks)
