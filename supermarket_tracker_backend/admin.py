from django.contrib import admin

from .models import Product, ProductMistake, ProductPrice, User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ["id", "email", "date_joined"]


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ["product_id", "name", "category", "store"]
    list_filter = ["store", "category", "unit"]
    readonly_fields = [
        "product_id",
        "store",
        "shop_unit_string",
    ]
    ordering = ["product_id", "store"]
    search_fields = ["product_id", "name"]


@admin.register(ProductPrice)
class ProductPrizeAdmin(admin.ModelAdmin):
    list_display = ["product", "date", "price"]
    list_filter = ["date"]
    readonly_fields = [
        "product",
        "date",
        "price",
    ]
    search_fields = ["product__product_id"]


@admin.register(ProductMistake)
class ProductMistakeAdmin(admin.ModelAdmin):
    list_display = ["product_id"]
    search_fields = ["product_id"]
