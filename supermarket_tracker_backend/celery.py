import os

from celery import Celery
from celery.schedules import crontab
from django.conf import settings

from connectors.models import Stores

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "supermarket_tracker_backend.settings.dev")

app = Celery("tasks", broker=settings.CELERY_BROKER_URL)  # type: ignore

app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()

app.conf.beat_schedule = {
    "fetch_store_data_ah": {
        "task": "supermarket_tracker_backend.tasks.fetch_store_data",
        "schedule": crontab(hour=2, minute=0),
        "args": (Stores.ALBERT_HEIJN.value),
    },
    "fetch_store_data_aldi": {
        "task": "supermarket_tracker_backend.tasks.fetch_store_data",
        "schedule": crontab(hour=2, minute=0),
        "args": (Stores.ALDI.value),
    },
    "fetch_store_data_coop": {
        "task": "supermarket_tracker_backend.tasks.fetch_store_data",
        "schedule": crontab(hour=2, minute=0),
        "args": (Stores.COOP.value),
    },
    "fetch_store_data_jumbo": {
        "task": "supermarket_tracker_backend.tasks.fetch_store_data",
        "schedule": crontab(hour=2, minute=0),
        "args": (Stores.JUMBO.value),
    },
}
