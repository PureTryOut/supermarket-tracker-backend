import io
from asyncio import TimeoutError
from datetime import date

from aiohttp import ClientResponse, ClientResponseError, ClientSession
from aiohttp.client_exceptions import ClientConnectionError
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.core.files.images import ImageFile
from django.db import models
from django.db.models.fields.files import ImageFieldFile
from django.utils.translation import gettext as _
from rest_framework import status

from connectors.models import CategoryOptions, Stores


class UserManager(BaseUserManager):
    use_in_migrations = True

    def __create_user(self, email, password, **kwargs):
        if email is None:
            raise ValueError("An email is required")

        email = self.normalize_email(email)
        user = self.model(email=email, **kwargs)
        user.set_password(password)  # type: ignore
        user.save(using=self._db)

        return user

    def create_user(self, email, password=None, **kwargs):
        kwargs.setdefault("is_staff", False)
        kwargs.setdefault("is_superuser", False)
        return self.__create_user(email, password, **kwargs)

    def create_superuser(self, email, password, **kwargs):
        kwargs.setdefault("is_staff", True)
        kwargs.setdefault("is_superuser", True)

        if kwargs.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if kwargs.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self.__create_user(email, password, **kwargs)


class User(AbstractUser):
    username = None  # type: ignore
    email = models.EmailField(verbose_name=_("e-mail address"), unique=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = UserManager()  # type: ignore

    last_modified = models.DateTimeField(auto_now=True)


class CategoryChoices(models.TextChoices):
    FRUIT_AND_VEGETABLES = (
        CategoryOptions.FRUIT_AND_VEGETABLES.value,
        _("Fruit and vegetables"),
    )
    BREAD_AND_PASTRIES = (
        CategoryOptions.BREAD_AND_PASTRIES.value,
        _("Bread and pastries"),
    )
    BEVERAGES = CategoryOptions.BEVERAGES.value, _("Beverages")
    REFRIGERATED_GOOD = (
        CategoryOptions.REFRIGERATED_GOOD.value,
        _("Refrigerated products"),
    )
    FROZEN = CategoryOptions.FROZEN.value, _("Frozen products")
    STAPLE_FOOD = CategoryOptions.STAPLE_FOOD.value, _("Staple food")
    SWEET_AND_SALTY = (
        CategoryOptions.SWEET_AND_SALTY.value,
        _("Sweet and salty products"),
    )
    CARE = CategoryOptions.CARE.value, _("Care products")
    HOUSEHOLD_AND_DOMESTIC_ANIMALS = (
        CategoryOptions.HOUSEHOLD_AND_DOMESTIC_ANIMALS.value,
        _("Household and domestic animal products"),
    )
    UNKNOWN = CategoryOptions.UNKNOWN.value, _("Unknown")


class StoreChoices(models.TextChoices):
    ALBERT_HEIJN = Stores.ALBERT_HEIJN.value, _("Albert Heijn")
    JUMBO = Stores.JUMBO.value, _("Jumbo")
    COOP = Stores.COOP.value, _("COOP")
    ALDI = Stores.ALDI.value, _("Aldi")


class Product(models.Model):
    product_id = models.CharField(
        max_length=20,
        null=False,
        blank=False,
        # Some stores store ID for example as EAN codes, those won't be unique between them
        # The only unique ID we can be sure of is our own
        unique=False,
        help_text="The ID of the product as used by the store",
    )
    store = models.CharField(
        max_length=20,
        choices=StoreChoices.choices,
        null=False,
        blank=False,
        help_text="Which store the product comes from",
    )
    name = models.CharField(
        max_length=200,
        null=False,
        blank=False,
        help_text="The name of the product as shown in apps and websites",
    )
    unit = models.CharField(
        max_length=10,
        null=False,
        blank=False,
        help_text="The unit in which the store saves the product. "
        "Note this is not exactly as the store stores it but normalized so we can use it for calculations",
    )
    quantity = models.IntegerField(
        null=False,
        blank=False,
        help_text="The quantity of the unit this product is sold as",
    )
    shop_unit_string = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        help_text="We try to parse the unit and quantity but since they're manually inserted into the store's systems "
        "it's very failure prone. This contains the original string as currently used by the store",
    )
    ean_code = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        help_text="The EAN code for this product. "
        "Is only filled after a user searches for a EAN code using our endpoints",
    )
    image_url = models.URLField(
        null=True,
        blank=True,
    )
    image = models.ImageField(
        upload_to="product_images",
        null=True,
        blank=True,
    )

    category = models.CharField(
        max_length=100,
        choices=CategoryChoices.choices,
        null=False,
        blank=False,
        help_text="The category this product falls under",
    )

    class Meta:
        unique_together = ["product_id", "store"]

    def __str__(self):
        return f"({self.id}, {self.store} {self.product_id}) {self.name}"

    async def get_image(self, client: ClientSession) -> ImageFieldFile | None:
        if self.image:
            return self.image

        if self.image_url is None:
            return None

        # Try to download the image from the store and save it locally
        # so we can return it next time
        try:
            async with ClientSession() as client:
                response: ClientResponse = await client.get(
                    url=self.image_url,
                    timeout=10,
                    headers={
                        # Some stores, like the Jumbo, complain if you don't have some normal-looking User-Agent
                        "User-Agent": "Mozilla/5.0 (Linux; Android 11; M2101K7AG) AppleWebKit/537.36 "
                        "(KHTML, like Gecko) Chrome/96.0.4664.45 Mobile Safari/537.36",
                    },
                )

                try:
                    response.raise_for_status()
                except ClientResponseError as exc:
                    if exc.status == status.HTTP_404_NOT_FOUND:
                        pass
                    else:
                        raise exc

                content_type: str = response.headers["Content-Type"]

                extension: str | None = None
                match content_type:
                    case "image/jpeg":
                        extension = ".jpg"
                    case "image/png":
                        extension = ".png"

                try:
                    image = await response.content.read()
                    self.image = ImageFile(
                        io.BytesIO(image),
                        name=f"{self.id}_{self.name}{extension}",
                    )
                    await self.asave()
                except ClientConnectionError:
                    # We'll just try again next time
                    pass
        except TimeoutError:
            # We'll just try again next time
            pass

        return self.image if self.image else None


class ProductPrice(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    date = models.DateField(default=date.today)
    price = models.IntegerField(help_text=_("The price in whole cents"))

    class Meta:
        unique_together = ["product", "date"]


class ProductMistake(models.Model):
    """
    Since we rely on users to submit EAN codes (there is no database containing all the products we need,
    especially not a free to use one) these are prone to mistakes.
    To make sure users can indicate the code is wrong and should either be removed or linked to a different product,
    we allow them to mark mistakes and leave a message we can act upon.
    Although this is mainly meant for those EAN codes, it can also be used to notify us about any other product mistake.
    """

    product = models.ForeignKey(Product, on_delete=models.CASCADE, help_text="The product containing a mistake")
    message = models.TextField(
        null=False,
        blank=False,
        help_text="A message the user can leave indicating what's wrong with the linked product",
    )
    email = models.EmailField(
        null=True,
        blank=True,
        help_text="An optional email so we can contact the user to ask something about the mistake",
    )

    class Meta:
        verbose_name = "Product submitted mistake"
        verbose_name_plural = "Product submitted mistakes"
