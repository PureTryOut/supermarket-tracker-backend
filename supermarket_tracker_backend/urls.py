from django.contrib import admin
from django.urls import path
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView

from . import views

urlpatterns = [
    path("admin/", admin.site.urls),
    path(
        "api/",
        SpectacularSwaggerView.as_view(url_name="schema", versioning_class=None),
        name="swagger-ui",
    ),
    path("api/schema/", SpectacularAPIView.as_view(versioning_class=None), name="schema"),
    path("api/products", views.ListProductsAPIView.as_view(), name="product-list"),
    path("api/products/<str:product_id>/prices", views.ListProductPricesAPIView.as_view(), name="product-prices"),
    path("api/products/<str:product_id>/image", views.ProductImageAPIView.as_view(), name="product-image"),
    path(
        "api/products/<str:product_id>/submit-mistake",
        views.SubmitProductMistakeAPIView.as_view(),
        name="product-submit-mistake",
    ),
]
