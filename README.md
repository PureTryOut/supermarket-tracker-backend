# Supermarket tracker

A grocery chain price search application.
It fetches product data from some big grocery chains (currently just the Dutch stores Albert Heijn and Jumbo) and let's you search through them.

This repository is just a backend, a client has yet to be made.
This backend is written in Python using Django.

The project is heavily inspired by https://github.com/badlogic/heissepreise.

## Adding a new grocery chain

The data is fetched through a cron job ran every night.
Adding a new grocery store is easy, look at the existing implementations in `connectores/stores` for an example.
If you add a new store remember to add it to `supermarket_tracker_backend.models.StoreChoices` so it can be saved to the database and to `supermarket_tracker_backend.cron.CronJobFetchStoreData.do` to actually enable the data fetcher.

## Run the cron job

Cron jobs are done using Celery.
Make sure you have a worker for it running:

```sh
celery -A supermarket_tracker_backend worker --loglevel=INFO
```

Cron jobs will automatically run at set times but if you want to run a task manually you can do so by executing the task from a Python shell:

```python
from supermarket_tracker_backend.tasks import fetch_store_data
fetch_store_data.delay()
```